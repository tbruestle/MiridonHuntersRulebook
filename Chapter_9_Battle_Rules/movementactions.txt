<h3>Movement Actions:</h3>
<br/><b>Sidestep:</b>
<br/>
<br/>This [Movement Action] can be performed to move 5 feet in a straight line.
<br/>
<br/>The character can move in any [Direction].
<br/>
<br/>After the end of the [Sidestep], the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>
<br/>
<br/><b>Fast Sidestep:</b>
<br/>
<br/>This [Movement Action] can be performed to move 10 feet in a straight line.
<br/>
<br/>The character can move in any [Direction].
<br/>
<br/>After the end of the [Fast Sidestep], the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>
<br/>
<br/><b>Charge:</b>
<br/>
<br/>This [Movement Action] can be performed to move up to 10 feet in a straight line.
<br/>
<br/>The [Direction] the character can move is either the [Direction] the character is facing at the start of the [Action Block], or any [Direction] up to 60° off that [Direction].
<br/>
<br/>After the end of the [Charge], the [Direction] the character is facing will be the direction the character moved.
<br/>
<br/>If [Charge] is performed before an [Melee Attack] or [Melee Combo Attack] in the same [Action Block], then [Damage Dealt] of the [Melee Attack] or [Melee Combo Attack] will increase by attacking weapon's [Weapon Weight] on a successful a [To Hit Roll].
<br/>
<br/>
<br/>
<br/><b>Fast Charge:</b>
<br/>
<br/>This [Movement Action] can be performed to move up to 20 feet in a straight line.
<br/>
<br/>The [Direction] the character can move is either the [Direction] the character is facing at the start of the [Action Block], or any [Direction] up to 60° off that [Direction].
<br/>
<br/>After the end of the [Fast Charge], the [Direction] the character is facing will be the direction the character moved.
<br/>
<br/>If [Fast Charge] is performed before an [Melee Attack] or [Melee Combo Attack] in the same [Action Block], then [Damage Dealt] of the [Melee Attack] or [Melee Combo Attack] will increase by twice attacking weapon's [Weapon Weight] on a successful a [To Hit Roll].
<br/>
<br/>
<br/>
<br/><b>Turn:</b>
<br/>
<br/>When this [Movement Action] is performed, the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>
<br/>
<br/><b>Teleport:</b>
<br/>Pre-requisite: [Rank] 1 [Teleport]
<br/>
<br/>This [Movement Action] can be performed to move up to 5 feet per [Rank] in [Teleport].
<br/>
<br/>The character can move in any [Direction].
<br/>
<br/>This [Movement Action] can bypass any obstacle.
<br/>
<br/>The character spends 1 [Current Ghost Power] for every 1 foot moved.
<br/>
<br/>After the end of the [Teleport], the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>The character must have [Rank]s in [Teleport] to perform [Teleport].
<br/>
<br/>
<br/>
<br/><b>Fly:</b>
<br/>Pre-requisite: [Gale Burst] with [Flying] cast on character
<br/>
<br/>This [Movement Action] can be performed to move up to 10 feet in any direction, including vertical directions.
<br/>
<br/>The character can also split this movement in multiple directions.
<br/>
<br/>After the end of the [Fly], the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>The character must have had [Gale Burst] with [Flying] cast on character.
<br/>
<br/>
<br/>
<br/><b>Advanced Fly:</b>
<br/>Pre-requisite: [Gale Burst] with [Advanced Flying] cast on character
<br/>
<br/>This [Movement Action] can be performed to move up to 15 feet in any direction, including vertical directions.
<br/>
<br/>The character can also split this movement in multiple directions.
<br/>
<br/>After the end of the [Advanced Fly], the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>The character must have had [Gale Burst] with [Advanced Flying] cast on character.
<br/>
<br/>
<br/>
<br/><b>Greater Fly:</b>
<br/>Pre-requisite: [Gale Burst] with [Greater Flying] cast on character
<br/>
<br/>This [Movement Action] can be performed to move up to 20 feet in any direction, including vertical directions.
<br/>
<br/>The character can also split this movement in multiple directions.
<br/>
<br/>After the end of the [Greater Fly], the [Direction] the character is facing can be chosen as any [Direction].
<br/>
<br/>The character must have had [Gale Burst] with [Greater Flying] cast on character.
<br/>
<br/>
<br/>
<br/><b>Restore Speed Points:</b>
<br/>
<br/>This [Movement Action] restores 5 [Current Speed Points] to the character, plus 1 [Current Speed Point] for every [Rank] in [Restore Speed Points Boost]. The [Current Speed Points] cannot exceed the character's [Maximum Battle Speed Points].